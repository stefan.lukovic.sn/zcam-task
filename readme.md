# ZCAM test REST api

For this task I created Restfull application for Ordering system. There is two services `/orders` and `/articles`
with context path `/api`. Use case is pretty flexible, so you can create order and after that articles with reference
that order or to create articles, and then on create or update order add reference to created articles. 

Rest service `/api/articles/{id}` return data about saved article with additional data for valid **productCode**
of IKEA product from external service.

On POST methods and response status 201 you can find URL to created data in response header `location` 

Technology stack
============

* Java EE
* MongoDB
* Wildfly
* Maven
* Keycloak


Run
-------------------

Prerequisites:

* JDK 8 - check `java -version`
* Maven 3.6.0 or newer - check `mvn -v`

To build project and start WildFly server

Run command:
> mvn clean install && mvn wildfly:run

- It will start Wildfly server with MongoDB 

API Documentation
============
* API documentation can be found at [Swagger UI](http://localhost:8080/docs/)

Postman
============
* Collection with all available requests examples can be imported from [Postman collection](https://www.getpostman.com/collections/2a4e5d425d89f7cb92a0)

Authorization
============

Rest services /api/articles are secured, and to execute them is necessary to add authorization header with **Bearer token**.

Bearer token is available in:

* "Auth token" request in postman collection:
![img.png](img.png)

* via CURL:
curl --location --request POST 'https://auth.ikeadostava.me/auth/realms/public/protocol/openid-connect/token' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'grant_type=password' \
--data-urlencode 'client_id=zcam-client' \
--data-urlencode 'username=zcamtest' \
--data-urlencode 'password=zcam' \
--data-urlencode 'realm=public'


