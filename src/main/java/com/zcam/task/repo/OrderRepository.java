package com.zcam.task.repo;

import com.zcam.task.model.Article;
import com.zcam.task.model.Order;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * {@link Order} CRUD operations.
 *
 * @author Stefan
 */
@ApplicationScoped
public class OrderRepository {

	@PersistenceContext
	private EntityManager entityManager;

	public Order create(Order order) {
		entityManager.persist( order );
		return order;
	}

	public Order get(String id) {
		return entityManager.find( Order.class, id );
	}

	public List<Order> getAll() {
		return entityManager.createQuery( "FROM Order p", Order.class ).getResultList();
	}

	public Order save(Order order) {
		return entityManager.merge( order );
	}

	public void remove(String id) {
		Order order = get(id);
		entityManager.remove( order );
		for ( Article article : order.getArticles() ) {
			entityManager.remove(article);
		}
	}
}
