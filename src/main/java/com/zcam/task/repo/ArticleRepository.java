package com.zcam.task.repo;

import com.zcam.task.model.Article;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * {@link Article} CRUD operations.
 *
 * @author Stefan
 */
@ApplicationScoped
public class ArticleRepository {

	@PersistenceContext
	private EntityManager entityManager;

	public Article create(Article article) {
		if ( article.getOrder() != null ) {
			article.getOrder().getArticles().add( article );
		}

		entityManager.persist( article );

		return article;
	}

	public Article get(String id) {
		return entityManager.find( Article.class, id );
	}

	public List<Article> getAll() {
		return entityManager.createQuery( "FROM Article h", Article.class ).getResultList();
	}

	public void remove(String id) {
		Article article = entityManager.getReference(Article.class,id);
		entityManager.remove(article);
		article.getOrder().getArticles().remove( article );
	}
}
