package com.zcam.task.model;

import org.hibernate.annotations.GenericGenerator;
//import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Article entity class.
 *
 * @author Stefan
 */
@Entity
//@Indexed
public class Article {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private Integer quantity;
	private BigDecimal price;
	private String productCode;

	@ManyToOne
	private Order order;


	public Article() {
	}

	public Article(String id) {
		this.id = id;
	}

	public Article(Integer quantity, BigDecimal price, String productCode) {
		this.quantity = quantity;
		this.price = price;
		this.productCode = productCode;
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
}
