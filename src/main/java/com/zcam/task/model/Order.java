package com.zcam.task.model;

import org.hibernate.annotations.GenericGenerator;
//import org.hibernate.search.annotations.Indexed;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * An order entity class.
 *
 * @author Stefan
 */
@Entity
//@Indexed
public class Order {

	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	private String clientName;

	@Temporal( TemporalType.DATE )
	private Date date;

	@OneToMany(
			mappedBy = "order",
			cascade = { CascadeType.PERSIST, CascadeType.MERGE },
			fetch = FetchType.EAGER
	)
	private Set<Article> articles = new HashSet<>();

	public Order() {
	}

	public Order(String id) {
		this.id = id;
	}

	public Order(String clientName, Date date) {
		this.clientName = clientName;
		this.date = date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Set<Article> getArticles() {
		return articles;
	}

	public void setArticles(Set<Article> articles) {
		this.articles = articles;
	}
}
