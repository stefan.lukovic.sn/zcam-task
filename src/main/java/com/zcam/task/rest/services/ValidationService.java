package com.zcam.task.rest.services;

import com.zcam.task.model.Order;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Service for validations check.
 *
 * @author Stefan
 */
public class ValidationService {

    private static Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

     public static <T> List<String> getValidationErrors(T bean) {
        List<String> messages = new ArrayList<>();

        Set<ConstraintViolation<T>> violations = validator.validate(bean);
        for (ConstraintViolation<T> violation : violations) {
            messages.add(violation.getMessage());
        }
        return messages;
    }


}