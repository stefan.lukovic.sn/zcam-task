package com.zcam.task.rest.services;

import com.zcam.task.model.Order;
import com.zcam.task.repo.OrderRepository;
import com.zcam.task.rest.mapper.ResourceMapper;
import com.zcam.task.rest.mapper.UriMapper;
import com.zcam.task.rest.model.OrderDocument;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import java.net.URI;
import java.util.List;
/**
 * Service for managing {@link Order}s.
 *
 * @author Stefan
 */
@ApplicationScoped
public class OrderService {

    @Inject
    private OrderRepository orderRepository;

    @Inject
    private ResourceMapper mapper;

    @Inject
    private UriMapper uris;


    public List<OrderDocument> listOrders() {
        List<Order> orders = orderRepository.getAll();
        List<OrderDocument> orderDocuments = mapper.toOrderDocuments( orders );

        return orderDocuments;
    }

    public URI createOrder(OrderDocument request) {

       /* List<String> greske = ValidationService.vratiGreskeValidacije(request);
        if (!greske.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).entity(greske).build();
        } TODO isto kao i za artikle, mozda i izmestiti mapiranje i validaciju u controler*/
        Order order = orderRepository.create( mapper.toOrder( request ) );
        return uris.toUri( order );
    }

    public OrderDocument getOrder(String id) {
        Order order = orderRepository.get( id );
        if ( order == null ) {
            return null;
        }
        else {
            return mapper.toOrderDocument(order);
        }
    }

    public Order updateOrder(OrderDocument request, @PathParam("id") String id) {
        Order order = orderRepository.get( id );
        if ( order == null ) {
            return null;
        }

        mapper.updateOrder( request, order );

        return order;
    }

    public void deleteOrder(String id) {
        orderRepository.remove( id );
    }
}