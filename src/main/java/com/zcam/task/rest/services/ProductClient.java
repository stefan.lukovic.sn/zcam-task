package com.zcam.task.rest.services;

import com.zcam.task.rest.model.ProductDTO;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * REST client for reading article data.
 *
 * @author Stefan
 */
@ApplicationScoped
public class ProductClient {

    private static final String REST_URI
            = "https://ikea-shop-me.herokuapp.com/shopme/catalog/findByCode/";

    private Client client = ClientBuilder.newClient();

    public ProductDTO getProductInfo(String productCode) {
        List<ProductDTO> result = client
                .target(REST_URI)
                .path(productCode)
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<List<ProductDTO>>() {});

        if(result!=null && !result.isEmpty()){
            return result.get(0);
        }
        return null;
    }
}