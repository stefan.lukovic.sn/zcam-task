package com.zcam.task.rest.services;

import com.sun.jndi.toolkit.url.Uri;
import com.zcam.task.model.Article;
import com.zcam.task.model.Order;
import com.zcam.task.repo.ArticleRepository;
import com.zcam.task.rest.mapper.ResourceMapper;
import com.zcam.task.rest.mapper.UriMapper;
import com.zcam.task.rest.model.ArticleDocument;
import com.zcam.task.rest.model.ProductDTO;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

/**
 * Service for managing {@link Article}s.
 *
 * @author Stefan
 */
@ApplicationScoped
public class ArticleService {

    @Inject
    private ArticleRepository articleRepository;

    @Inject
    private ResourceMapper mapper;

    @Inject
    private UriMapper uris;

    @Inject
    private ProductClient productClient;

    public List<ArticleDocument> listArticles() {
        List<Article> articles = articleRepository.getAll();
        List<ArticleDocument> articleDocuments = mapper.toArticleDocuments( articles );
        return articleDocuments;
    }

    public ProductDTO fetchProductData(String code) {
        ProductDTO productDTO = productClient.getProductInfo(code);
        return productDTO;
    }

    public URI createArticle(ArticleDocument request) throws Exception {
     /*   List<String> greske = ValidationService.vratiGreskeValidacije(request);
        if (!greske.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).entity(greske).build();
        } TODO dal baciti izuzetak*/
        Article article = articleRepository.create( mapper.toArticle( request ) );
        return uris.toUri( article );
    }

    public ArticleDocument getArticle(String id) {

        Article article = articleRepository.get( id );

        if ( article == null ) {
            return null;
        }
        else {
            ProductDTO productDTO =productClient.getProductInfo(article.getProductCode());
            ArticleDocument articleDocument = mapper.from( article, productDTO );
            return articleDocument;
        }
    }

    public Article updateArticle(ArticleDocument request, String id) {
        Article article = articleRepository.get( id );
        if ( article == null ) {
            return null;
        }

        mapper.updateArticle( request, article );

        if ( article.getOrder() != null ) {
            article.getOrder().getArticles().add( article );
        }

        return article;
    }

    public void deleteArticle(String id) {
        articleRepository.remove( id);
    }
}