package com.zcam.task.rest.model;

import com.zcam.task.model.Article;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.net.URI;

/**
 * The {@link Article} representation used during REST calls.
 *
 * @author Stefan
 */
public class ArticleDocument {

	private URI order;

	@NotNull(message = "quantity can not be null")
	@Min(value = 1, message = "quantity has to be greater then 0")
	private Integer quantity;

	@NotNull(message = "price can not be null")
	@Min(value = 0, message = "price has to be greater then 0")
	private BigDecimal price;

	@NotNull(message = "productCode can not be null")
	@Pattern(regexp = "^[0-9]{3}\\.{0,1}[0-9]{3}\\.{0,1}[0-9]{2}$", message = "productCode should be formated like '000.000.00'")
	private String productCode;

	private String productName;
	private String imageURL;

	public URI getOrder() {
		return order;
	}

	public void setOrder(URI order) {
		this.order = order;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
}