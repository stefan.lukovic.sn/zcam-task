package com.zcam.task.rest.model;

import com.zcam.task.model.Order;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.net.URI;
import java.util.Date;
import java.util.Set;

/**
 * The {@link Order} representation used during REST calls.
 *
 * @author Stefan
 */
public class OrderDocument {


	@NotNull(message = "clientName can not be null")
	private String clientName;

	@NotNull(message = "date can not be null")
	@Pattern(regexp = "^\\d{4}(-)(((0)[0-9])|((1)[0-2]))(-)([0-2][0-9]|(3)[0-1])$", message = "date has to be formated as yyyy-mm-dd")
	private String date;
	private Set<URI> articles;

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Set<URI> getArticles() {
		return articles;
	}

	public void setArticles(Set<URI> articles) {
		this.articles = articles;
	}
}
