package com.zcam.task.rest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO for external API call.
 *
 * @author Stefan
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductDTO {

    @JsonProperty("nameFull")
    private String productName;
    @JsonProperty("imageUrl")
    private String imageURL;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductDTO{");
        sb.append("productName='").append(productName).append('\'');
        sb.append(", imageURL='").append(imageURL).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
