package com.zcam.task.rest.resource;

import com.zcam.task.model.Article;
import com.zcam.task.repo.ArticleRepository;
import com.zcam.task.rest.mapper.ResourceMapper;
import com.zcam.task.rest.mapper.UriMapper;
import com.zcam.task.rest.model.ArticleDocument;
import com.zcam.task.rest.model.ProductDTO;
import com.zcam.task.rest.services.ArticleService;
import com.zcam.task.rest.services.ProductClient;
import com.zcam.task.rest.services.ValidationService;
import io.swagger.annotations.*;

import javax.annotation.security.DenyAll;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response.Status;
import java.security.Principal;
import java.util.List;

/**
 * REST resource for managing {@link Article}s.
 *
 * @author Stefan
 */
@Path("/articles")
@Stateless
@Produces("application/json")
@Consumes("application/json")
@Api(value = "Articles API", tags={"articles"}, authorizations = @Authorization("jwt-auth"))
@PermitAll
public class ArticleController {

	@Inject
	private ArticleService articleService;

	@GET
	@Path("/")
	@ApiOperation(value = "Fetch all articles",
			authorizations = {
					@Authorization(value = "Only the user that submitted the request can get it")
			})
	@ApiResponses({
			@ApiResponse(code=200, message="Success"),
			@ApiResponse(code = 403, message = "Client is not authorized to make this request.")
	})
	@RolesAllowed("zcamrole")
	public Response listArticles(@Context SecurityContext securityContext,
								 @Context UriInfo uri) {
		return Response.ok( articleService.listArticles() ).build();
	}

	@POST
	@Path("/")
	@ApiOperation(value = "Create new article")
	@ApiResponses({
			@ApiResponse(code=201, message="Created")
	})
	@RolesAllowed("zcamrole")
	public Response createArticle(ArticleDocument request) throws Exception {
		List<String> errors = ValidationService.getValidationErrors(request);
		if (!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}

		return Response.created( articleService.createArticle(request) ).build();
	}

	@GET
	@Path("/{id}")
	@ApiOperation(value = "Get existing article")
	@ApiResponses({
			@ApiResponse(code=200, message="Success"),
			@ApiResponse(code=404, message="Not found")
	})
	@RolesAllowed("zcamrole")
	public Response getArticle(@PathParam("id") String id) {
		ArticleDocument article = articleService.getArticle(id);
		if ( article == null ) {
			return Response.status( Status.NOT_FOUND ).build();
		}
		else {
			return Response.ok( article ).build();
		}
	}

	@PUT
	@Path("/{id}")
	@ApiOperation(value = "Updates existing article")
	@ApiResponses({
			@ApiResponse(code=200, message="Updated"),
			@ApiResponse(code=404, message="Not found")
	})
	@RolesAllowed("zcamrole")
	public Response updateArticle(ArticleDocument request, @PathParam("id") String id) {

		List<String> errors = ValidationService.getValidationErrors(request);
		if (!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}

		Article article = articleService.updateArticle(request, id);
		if ( article == null ) {
			return Response.status( Status.NOT_FOUND ).build();
		}
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "Delete existing article")
	@ApiResponses({
			@ApiResponse(code=200, message="Deleted"),
			@ApiResponse(code=404, message="Not found")
	})
	@RolesAllowed("zcamrole")
	public Response deleteArticle(@PathParam("id") String id) {
		articleService.deleteArticle( id);
		return Response.ok().build();
	}
}
