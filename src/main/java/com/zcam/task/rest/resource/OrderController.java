package com.zcam.task.rest.resource;

import com.zcam.task.model.Order;
import com.zcam.task.repo.OrderRepository;
import com.zcam.task.rest.mapper.ResourceMapper;
import com.zcam.task.rest.mapper.UriMapper;
import com.zcam.task.rest.model.OrderDocument;
import com.zcam.task.rest.services.OrderService;
import com.zcam.task.rest.services.ValidationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import java.util.List;

/**
 * REST resource for managing {@link Order}s.
 *
 * @author Stefan
 */
@Path("/orders")
@Produces("application/json")
@Consumes("application/json")
@Stateless
@Api(tags = {"orders"}, value = "/orders")
public class OrderController {

	@Inject
	private OrderService orderService;

	@Context
	private UriInfo uriInfo;

	@GET
	@Path("/")
	@ApiOperation(value = "Fetch all orders")
	@ApiResponses({
			@ApiResponse(code=200, message="Success")
	})
	public Response listOrders() {
		return Response.ok( orderService.listOrders() ).build();
	}

	@POST
	@Path("/")
	@ApiOperation(value = "Create new order")
	@ApiResponses({
			@ApiResponse(code=201, message="Created")
	})
	public Response createOrder(OrderDocument request) {

		List<String> errors = ValidationService.getValidationErrors(request);
		if (!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}

		return Response.created( orderService.createOrder(request) ).build();
	}

	@GET
	@Path("/{id}")
	@ApiOperation(value = "Get existing order")
	@ApiResponses({
			@ApiResponse(code=200, message="Success"),
			@ApiResponse(code=404, message="Not found")
	})
	public Response getOrder(@PathParam("id") String id) {
		OrderDocument order = orderService.getOrder(id);
		if ( order == null ) {
			return Response.status( Status.NOT_FOUND ).build();
		}
		else {
			return Response.ok( order ).build();
		}
	}

	@PUT
	@Path("/{id}")
	@ApiOperation(value = "Updates existing order")
	@ApiResponses({
			@ApiResponse(code=200, message="Updated"),
			@ApiResponse(code=404, message="Not found")
	})
	public Response updateOrder(OrderDocument request, @PathParam("id") String id) {
		List<String> errors = ValidationService.getValidationErrors(request);
		if (!errors.isEmpty()) {
			return Response.status(Status.BAD_REQUEST).entity(errors).build();
		}

		Order order = orderService.updateOrder(request, id);
		if ( order == null ) {
			return Response.status( Status.NOT_FOUND ).build();
		}
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	@ApiOperation(value = "Delete existing order")
	@ApiResponses({
			@ApiResponse(code=200, message="Deleted"),
			@ApiResponse(code=404, message="Not found")
	})
	public Response deleteOrder(@PathParam("id") String id) {
		orderService.deleteOrder(id);
		return Response.ok().build();
	}

	// Consumed by ResourceMapper
	@RequestScoped
	@javax.enterprise.inject.Produces
	public UriInfo produceUriInfo() {
		return uriInfo;
	}
}
