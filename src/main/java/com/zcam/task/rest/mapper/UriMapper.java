package com.zcam.task.rest.mapper;

import java.net.URI;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.zcam.task.model.Article;
import com.zcam.task.model.Order;
import com.zcam.task.rest.resource.ArticleController;
import com.zcam.task.rest.resource.OrderController;
import org.mapstruct.TargetType;

/**
 * Convert a {@link Order} or a {@link Article} into the corresponding {@link URI} that can be called to execute CRUD operations on that order.
 *
 * For example, the {@link URI} to get or update a {@link Order} could be:
 * <p>
 * http://127.0.0.1:8080/zcam-test/orders/68c6b528-a13b-484f-be38-7716667a7c85
 * <p>
 * where '68c6b528-a13b-484f-be38-7716667a7c85' is the id of the order.
 *
 * @author Stefan
 *
 */
@ApplicationScoped
public class UriMapper {

	@Inject
	private UriInfo uriInfo;

	@PersistenceContext
	private EntityManager entityManager;

	public URI toUri(Order order) {
		return order == null ? null : UriBuilder.fromUri( uriInfo.getBaseUri() ).path( OrderController.class ).path( "/{id}" ).build( order.getId() );
	}

	public URI toUri(Article article) {
		return UriBuilder.fromUri( uriInfo.getBaseUri() ).path( ArticleController.class ).path( "/{id}" ).build( article.getId() );
	}


	public <T> T load(URI uri, @TargetType Class<T> entityType) {
		if(uri==null){
			return null;
		}
		String id = toId( uri );
		return entityManager.find( entityType, id );
	}

	public String toId(URI uri) {
		String path = uri.getPath();
		return path.substring( path.lastIndexOf( "/" ) + 1 );
	}
}
