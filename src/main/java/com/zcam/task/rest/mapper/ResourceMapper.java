package com.zcam.task.rest.mapper;

import java.util.List;

import com.zcam.task.model.Article;
import com.zcam.task.model.Order;
import com.zcam.task.rest.model.ArticleDocument;
import com.zcam.task.rest.model.OrderDocument;
import com.zcam.task.rest.model.ProductDTO;
import org.mapstruct.InheritConfiguration;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

/**
 * A MapStruct-generated mapper for orders and articles.
 * <p>
 * Contains the methods for the conversion between {@link OrderDocument} and {@link Order} or {@link Article} and
 * {@link ArticleDocument}. It also contains the methods to update a {@link Order} or {@link Article} using the values in a
 * {@link OrderDocument} or {@link ArticleDocument}.
 *
 * @author Stefan
 */
@Mapper(uses = UriMapper.class, componentModel = "cdi", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ResourceMapper {


	@Mapping(source = "date", target = "date", dateFormat = "yyyy-MM-dd")
	OrderDocument toOrderDocument(Order order);

	@Mapping(target = "articles", ignore = true)
	@Mapping(source = "date", target = "date", dateFormat = "yyyy-MM-dd")
	Order toOrder(OrderDocument orderDocument);

	@InheritConfiguration(name = "toOrder")
	void updateOrder(OrderDocument orderDocument, @MappingTarget Order order);

	List<OrderDocument> toOrderDocuments(Iterable<Order> orders);

	ArticleDocument toArticleDocument(Article article);

	@InheritInverseConfiguration(name = "toArticleDocument")
	Article toArticle(ArticleDocument articleDocument);

	@InheritConfiguration(name = "toArticle")
	void updateArticle(ArticleDocument articleDocument, @MappingTarget Article article);

	List<ArticleDocument> toArticleDocuments(Iterable<Article> articles);

	ArticleDocument from(Article article, ProductDTO productDTO);
}
