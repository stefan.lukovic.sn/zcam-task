package com.zcam.task.rest;


import io.swagger.annotations.ApiKeyAuthDefinition;
import io.swagger.annotations.SecurityDefinition;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.jaxrs.config.BeanConfig;

import javax.annotation.security.DeclareRoles;
import javax.servlet.ServletConfig;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

import org.eclipse.microprofile.auth.LoginConfig;

/**
 * A REST application for managing articles and orders.
 *
 * @author Stefan
 */
@LoginConfig(authMethod = "MP-JWT")
@ApplicationPath("/api")
@DeclareRoles({"zcamrole","USER"})
@SwaggerDefinition(securityDefinition = @SecurityDefinition(
        apiKeyAuthDefinitions = {
                @ApiKeyAuthDefinition(
                        key = "user",
                        name = HttpHeaders.AUTHORIZATION,
                        in = ApiKeyAuthDefinition.ApiKeyLocation.HEADER
                )
        }
))
public class RootApplication extends Application {

    public RootApplication(@Context ServletConfig servletConfig) {
        super();

        BeanConfig beanConfig = new BeanConfig();

        beanConfig.setVersion("1.0.0");
        beanConfig.setTitle("ZCAM API");
        beanConfig.setBasePath("/api");
        beanConfig.setResourcePackage("com.zcam.task");
        beanConfig.setScan(true);
    }
}
